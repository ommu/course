<?php
/**
 * course module config
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 28 February 2019, 13:20 WIB
 * @link https://bitbucket.org/ommu/course
 *
 */

return [
	'id' => 'course',
	'class' => ommu\course\Module::className(),
];